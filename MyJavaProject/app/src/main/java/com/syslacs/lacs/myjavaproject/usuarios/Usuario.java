package com.syslacs.lacs.myjavaproject.usuarios;

public class Usuario
{
    private Integer ID;
    private String Nombres;
    private String Apellidos;
    private String Email;
    private String Login;
    private String Password;

    UserDelegate delegate;

    public Integer getID() {
        return ID;
    }
+
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public void AutenticarUsuario(String login, String password)
    {
        // llamada asyncrona

        //espero espero espero

        Usuario user=null;
        delegate.UsuarioAutenticado(user);

    }

}